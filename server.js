const express = require('express')
const proxy = require('http-proxy-middleware')
const http = require('http')
const Api = require('./src/api')
const app = express()
const router = express.Router()
const publicPath = require('./vue.config').publicPath

let port = 8080
if (process.env.NODE_ENV === 'production') {
} else {
  console.info("Not Production.")
}


app.use(publicPath + '/api', Api.api)
app.use(publicPath, express.static("./dist"))


// Always put healthcheck on the root to let the server know we're OK.
app.get('/healthcheck', function(req, res){
  res.send({ status: "OK" });
});

app.all('/', function(req, res) {
	res.status(302)
	res.set('Location', publicPath)
	res.end()
});

http.createServer(app).listen(port);

console.info(`Starting on port ${port}.`)
