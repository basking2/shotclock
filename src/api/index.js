const express = require('express')

const api = new express.Router()
const WsBroadcast = require('../ws/broadcast')

api.use('/ws/hub/:id', WsBroadcast.handleWebSocketBroadcast)

module.exports = {
    api
}