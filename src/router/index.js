import Vue from 'vue'
import VueRouter from 'vue-router'
// import Home from '../views/Home.vue'
import ShotClock from '../views/ShotClock.vue'

Vue.use(VueRouter)

  const routes = [
  { 
    path: '/', 
    name: 'ShotClock', 
    component: ShotClock,
    props: { domain: "default" },
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },

  { // This route should be last.
    path: '/:domain', name: 'ShotClock', component: ShotClock, props: true, 
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
