const WebSocket = require('ws')

const wss = new WebSocket.Server({ noServer: true });
wss.on('error', ee => {
    console.info("Error", ee)
})

wss.on('headers', event => {
    console.info("Headers", event)
})

let BroadcastDomain = class {
    constructor() {
        this.sockets = []
    }

    addSocket(hub, ws) {
        this.sockets.push(ws)

        // ws.addEventListener('open')
        ws.addEventListener('close', (event) => {
            this.sockets = this.sockets.filter(v => v !== ws)
            ws.close()
        })

        ws.addEventListener('error', (event) => {
            this.sockets = this.sockets.filter(v => v !== ws)
            ws.close()
        })

        ws.addEventListener('message', (event) => {
            this.sockets.forEach(v => {
                if (v !== ws) {
                    v.send(event.data)
                }
            })
        })

    }

    destroy() {
        this.sockets.forEach((ws) => {
            ws.close("Destroyed.")
        })
        this.sockets = []
    }
}

let BroadcastHub = class {
    constructor() {
        this.global_domain = new BroadcastDomain()
        this.limit = 100
        this.domains = {}
        this.domain_history = []
    }

    addSocket(domain, ws) {
        if (domain) {
            if (this.domains[domain]) {
                this.domains[domain].addSocket(this, ws)
            } else {
                this.domains[domain] = new BroadcastDomain()
                this.domain_history.push(domain)
                this.domains[domain].addSocket(this, ws)
            }

            while (this.domain_history.length > this.limit) {
                var d = this.domain_history.shift()
                this.domains[d].destroy()
                delete this.domains[d]
            }
        } else {
            this.global_domain.addSocket(this, ws)
        }
    }
}

let hub = new BroadcastHub()

function handleWebSocketBroadcast(req, resp, next) {
    const head = req.body || ''
    const socket = req.socket
    const domain = req.params && req.params.domain
    const connectionHeader = (req.get('connection') || '').toLowerCase()
    const upgradeHeader = req.get('upgrade')

    console.info(`Processing request for domain ${domain}.`)
    console.info(`Connection:${connectionHeader} Upgrade:${upgradeHeader}`)

    if (connectionHeader == 'upgrade' && upgradeHeader) {
        console.info("Upgrading connection.")

        wss.handleUpgrade(req, socket, head, function done(ws) {
            wss.emit('connection', ws, req);
    
            hub.addSocket(domain, ws)
    
        });    
    }
    else {
        resp.json({
            "status": "ERROR",
            "message": "Upgrade or Connection headers missing. Not upgrading."
        })
    }

}

module.exports = {
    handleWebSocketBroadcast
}
  