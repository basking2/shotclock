const publicPath = '/shotclock'

module.exports = {
  publicPath,
  "transpileDependencies": [
    "vuetify"
  ],
  'devServer': {
    'proxy': {
      '^/shotclock/api': {
        'target': `http://localhost:8080`,
        'ws': true,
        'changeOrigin': true,
      }
    }
  }
}
